//Rextester.Program.Main is the entry point for your code. Don't change it.
//Compiler version 4.0.30319.17929 for Microsoft (R) .NET Framework 4.5

using System;
using System.Linq;

namespace Rextester
{
    public class Program
    {
        public static void Main(string[] args)
        { 
            string[] buah = {"apel","jeruk","apel","pepaya","pepaya","apel"};
            var output = buah.GroupBy(namaBuah => namaBuah)
                .Select(namaBuah => new {Buah=namaBuah.Key, jumlah=namaBuah.Count()});
            
            foreach (var namaBuah in output){
                Console.WriteLine("{0} = {1}", namaBuah.Buah, namaBuah.jumlah); 
            }
        }
    }
}